  (* Sum of f applied to elements in row i on a matrix *)

module SumMatrix

  use int.Int
  use my_matrix.Matrix as M

  type t  (* type of elements *)

  val (=) (x y : t) : bool ensures { result <-> x=y }

 (* function isEmpty (s: t) :bool *)
  predicate isEmpty  (s: t) 
  
  function f (x:t) : int  
  val f  (x:t) : int
    ensures { result = f x }

  axiom f_empty : 
     forall x: t. f x = 0 <-> isEmpty x

  axiom f_pos : 
     forall x: t. f x >=0

  let rec function sum (m : M.matrix t) (i jl jr: int) : int   (* returns \sum_{jl <= k < jr} f (get m i k) *)  
   requires { 0 <= i < M.rows m }
   requires { 0 <= jl <= jr <= M.columns m }
   returns  { res -> res>=0 }
   returns  { res -> forall k:int. jl <= k < jr -> res >= f (M.get m i k) }                   (* sum greater or equal *)
   returns  { res -> res = 0 -> (forall k:int. jl <= k < jr -> isEmpty (M.get m i k)) }       (* empty states      *)
   returns  { res -> res > 0 -> (exists k:int. jl <= k < jr && not(isEmpty(M.get m i k))) }   (* non empty states  *)
   returns  { res -> res > 0 -> (exists k:int. jl <= k < jr && f (M.get m i k) >0) }          (* non empty states  *)
   variant { jr - jl } = 
      if jr=jl then 0
      else if (jr-jl=1) then f (M.get m i jl)
      else f (M.get m i jl) + sum m i (jl+1) jr

  let rec lemma sum_definition (m : M.matrix t) (i jl jr : int) 
    requires { 0 <= i < M.rows m }
    requires { 0<= jl <= jr <= M.columns m }
    ensures  { jl < jr  -> sum m i jl jr = f (M.get m i jl) + sum m i (jl+1) jr }
(*  ensures  { sum m i jl jr = 1 -> (exists k:int. jl <= k < jr && f (M.get m i k) = 1) }*)
    (* distributed state *)
    ensures  { sum m i jl jr > 1 ->  
       ( (exists k:int. jl <= k < jr && f (M.get m i k) > 1) ||
         (exists k1 k2:int. jl <= k1 < k2 < jr && f (M.get m i k1) >0 && f (M.get m i k2) >0 ) ) }
    variant { jr - jl } 
     =
      if (jr-jl) = 1 then assert { sum m i jl jr = f (M.get m i jl) } ;
      if (jr-jl) > 1 then sum_definition m i (jl+1) jr
    

  let rec lemma sum_transitivity (g: M.matrix t) (i k jl jr : int)
     requires { 0 <= i < M.rows g }
     requires { 0<= jl <= k <= jr <= M.columns g }
     ensures  { sum g i jl jr = sum g i jl k + sum g i k jr }
     variant { k-jl }
    = 
       if k<jl then absurd;
       if k=jl then assert { true };
       if k=jl+1 then assert { sum g i jl k = f (M.get g i jl) };
       if (k-jl) > 1 then sum_transitivity g i k (jl+1) jr

   let rec lemma sum_right_extension (g:M.matrix t) (i jl jr : int)
     requires { 0 <= i < M.rows g }
     requires { 0 <= jl < jr <= M.columns g }
     ensures  { sum g i jl jr = f (M.get g i (jr-1)) + sum g i jl (jr-1) }
     variant { jr }
    = 
      if (jr-jl) = 1 then assert { true } ;
      if (jr-jl) > 1 then 
          (  assert { 0<= jl <= (jr-1) <= jr <= M.columns g  };
             assert {sum g i jl jr = sum g i jl (jr-1) + sum g i (jr-1) jr }; 
             sum_right_extension g i jl (jr-1))

 let rec lemma sum_eq (g1 g2 : M.matrix t) (i jl jr : int)
      requires { 0 <= i < M.rows g1 }
      requires { 0 <= i < M.rows g2 }
      requires { 0 <= jl < jr <= M.columns g1 }
      requires { 0 <= jl < jr <= M.columns g2 }
      requires { M.matrix_eq_row_slice g1 g2 i jl jr }
      ensures  { sum g1 i jl jr = sum g2 i jl jr } 
      variant { jr - jl }
    = 
       if (jr-jl) = 1 then ( 
                      assert { M.get g1 i jl = M.get g2 i jl  };
                      assert { sum g1 i jl jr = f (M.get g1 i jl) };
                      assert { sum g2 i jl jr = f (M.get g2 i jl) };
                      assert { sum g1 i jl jr = sum g2 i jl jr } );
       if (jr-jl) > 1 then (
                       assert { sum g1 i jl jr = f (M.get g1 i jl) + sum g1 i (jl+1) jr };
                       assert { sum g2 i jl jr = f (M.get g2 i jl) + sum g2 i (jl+1) jr };
                       sum_eq g1 g2 i (jl+1) jr )

  let lemma sum_update_greater_col (l l': M.matrix t) (i jl jr c:int) (v:t)
    requires { M.valid_index l i c }
    requires { 0 <= jl  <= jr <= c  } 
    requires { l' = M.update l i c v }
    ensures  { sum l i jl jr =  sum l' i jl jr }
    =
    assert { M.matrix_eq_row_slice l (M.update l i c v) i jl jr }

  let rec lemma sum_update_neq_row (l l': M.matrix t) (i i1 c jl jr:int) (v:t)
    requires { M.valid_index l i c }
    requires { 0 <= jl <= jr <= M.columns l } 
    requires { 0 <= i1 < M.rows l }
    requires { i1 <> i }
    requires { l' = M.update l i c v } 
    ensures  { sum l i1 jl jr =  sum l' i1 jl jr }
    = 
    assert { M.matrix_eq_row_slice l l' i1 jl jr }

  let rec lemma sum_update (l l': M.matrix t) (i c jl jr : int) (v:t)
    requires { M.valid_index l i c }
    requires { 0 <= jl < jr <= M.columns l } 
    requires { jl <= c < jr }
    requires { l' = M.update l i c v } 
    ensures  { (sum l' i jl jr)   = (sum l i jl jr) - (f (M.get l i c)) + (f v) }
    variant  { jr } =
       if jr=0 then absurd; 
       (* first possible value for m, such that (jr-1)=c *)
       if jr=c+1 then ( 
           assert { v = (M.get l' i c) = (M.get l' i (jr-1)) };
           assert { M.matrix_eq_row_slice l l' i jl c };
           assert { sum l' i jl jr = sum l' i jl c + sum l' i c jr };
           assert { sum l i jl jr = sum l i jl c + sum l i c jr });

       (* values to sum up after m[i,c], such that (jr-1)<>c *)       
       if jr>c+1 then (
           assert { v = (M.get l' i c) };
           assert { (M.get l' i (jr-1)) = (M.get l i (jr-1)) };
           sum_update l l' i c jl (jr-1) v )

meta remove_prop axiom sum_right_extension
meta remove_prop axiom sum_transitivity
meta remove_prop axiom f_empty
meta remove_prop axiom f_pos

end
