  (* Sum of f applied to elements of an array *)

module SumArray

  use int.Int
  use import array.Array as A
  use my_matrix.Matrix as M

  (* ---------------------------------------------------------------------------- *)
  (* Parameters *)

  type t  (* type of elements *)

  val (=) (x y : t) : bool ensures { result <-> x=y }

  (*function isEmpty (s: t) : bool *)
  predicate  isEmpty (s:t)

  function f (x:t) : int  
  val f  (x:t) : int
    ensures { result = f x }

  axiom f_empty : 
     forall x: t. f x = 0 <-> isEmpty x

  axiom f_pos : 
     forall x: t. f x >=0

  (* ---------------------------------------------------------------------------- *)

  clone summatrix.SumMatrix as P with
    type t = t,
    function f = f,
    (*function isEmpty = isEmpty *)
    predicate isEmpty = isEmpty

  val function tomatrix (ar:A.array t) : M.matrix t
      ensures  { result.M.rows = 1 /\ result.M.columns = A.length ar }
      ensures  { forall c:int. 0 <= c < result.M.columns ->
                 result.M.elts 0 c = ar[c] }

  let function sum (a : A.array t) (jl jr: int) : int   (* returns \sum_{jl <= k < jr} f (get m 0 k) *)  
   requires { 0 <= jl <= jr <= A.length a } =
   P.sum (tomatrix a) 0 jl jr

  lemma sum_definition1 : forall a:A.array t, jl jr:int.
    0 <= jl < jr <= A.length a ->
    sum a jl jr = f (a[jl]) + sum a (jl+1) jr

  (* The following takes a long time to prove *)
  lemma sum_definition2 : forall a:A.array t, jl jr:int.
    0 <= jl <= jr <= A.length a -> (* why <=? *)
    sum a jl jr = 1 -> (exists k:int. jl <= k < jr && f (a[k]) = 1)

  (* The following is not proved in a reasonable amount of time, and it is
     not clear that it is useful either
  lemma sum_definition3 : forall a:A.array t, jl:int, jr:int.
    0 <= jl <= jr <= A.length a -> (* why <=? *)
    (* distributed state *)
    sum a jl jr > 1 ->  
       ( (exists k:int. jl <= k < jr && f (a[k]) > 1) ||
         (exists k1 k2:int. jl <= k1 < k2 < jr && f (a[k1]) > 0 && f (a[k2]) > 0 ) ) *)

  lemma sum_transitivity : forall a:A.array t, k jl jr:int.
     0 <= jl <= k <= jr <= A.length a ->
     sum a jl jr = sum a jl k + sum a k jr

  lemma sum_right_extension : forall a:A.array t, jl jr:int.
     0 <= jl < jr <= A.length a ->
     sum a jl jr = f (a[jr-1]) + sum a jl (jr-1)

  (* For matrices this is in my_matrix, but not in the why3 standard library *)
  (* There is an array_eq_sub in ArrayEq, but that requires the arrays to have the same size *)
  predicate array_eq_slice (l l': A.array t) (jl jr: int) =
    forall c. jl <= c < jr -> l[c] = l'[c]

  lemma array_matrix_eq : forall a1:A.array t, a2:A.array t, jl jr:int.
     0 <= jl <= jr <= A.length a1 ->
     0 <= jl <= jr <= A.length a2 ->
     array_eq_slice a1 a2 jl jr <-> M.matrix_eq_row_slice (tomatrix a1) (tomatrix a2) 0 jl jr

  lemma sum_eq : forall a1:A.array t, a2:A.array t, jl jr:int.
      0 <= jl < jr <= A.length a1 ->
      0 <= jl < jr <= A.length a2 ->
      array_eq_slice a1 a2 jl jr ->
      sum a1 jl jr = sum a2 jl jr 

  let lemma sum_update_greater_col (a:A.array t) (jl jr c:int) (v:t)
    requires { 0 <= c < A.length a }
    requires { 0 <= jl <= jr <= c }
    ensures { sum a jl jr = sum (a[c <- v]) jl jr }
    =
    assert { array_eq_slice a (a[c <- v]) jl jr }

  let lemma sum_update (a:A.array t) (c jl jr:int) (v:t)
    requires { 0 <= c < A.length a }
    requires { 0 <= jl < jr <= A.length a }
    requires { jl <= c < jr }
    ensures { (sum (a[c <- v]) jl jr) = (sum a jl jr) - (f (a[c])) + (f v) }
    =
    assert { M.matrix_eq_row_slice (tomatrix (a[c <- v])) (M.update (tomatrix a) 0 c v) 0 jl jr }

  meta remove_prop lemma array_matrix_eq
  meta remove_prop axiom f_empty
  meta remove_prop axiom f_pos

end
