module State

 use export threads.Threads
 use int.Int
 use list.List
 use list.Length
 use list.HdTl
 use list.Mem
 use list.Elements
 use set.Fset as F
 use option.Option

   (* States : list of distinct threads *)
     
   type state = { contents : list thread;
                  ghost elems : F.fset thread }
      invariant  { distinct contents }
      invariant  { elements contents = elems }
      by { contents = Nil; elems = F.empty }

   predicate isEmpty (s : state) =  s.contents = Nil 

   let function init_state ()
    ensures { isEmpty result} = { contents = Nil; elems = F.empty }
  
   let constant empty_state : state = init_state () 
 
   let isEmpty (s : state)
     ensures { result <-> isEmpty s }
    = eqtlist s.contents Nil

   (* let function isEmpty (s : state) = eqtlist s.contents Nil *)

   lemma nonempty_has_one : forall l:list thread.
      not(l = Nil) -> exists t:thread. hd l = Some t && Mem.mem t l

   let rec tlmem (t:thread) (l:list thread) =
      returns { res -> res <-> Mem.mem t l }
      variant { l }
     match l with
     | Nil -> false
     | Cons hd tl -> if t = hd then true else tlmem t tl
   end

   let function smem (t:thread) (s: state) : bool =
     ensures { result <-> Mem.mem t s.contents }
     ensures { isEmpty s -> result=false }
     tlmem t s.contents

   lemma set_mem : forall t:thread, s:state.
     smem t s <-> F.mem t s.elems

   lemma not_smem_empty : forall t:thread, s:state.
       isEmpty s -> not (smem t s)

   let function head (s : state) : thread
      requires { not (isEmpty s) }
 (*   requires { s.contents <> Nil }*)
      ensures { hd s.contents = Some result }
      returns { res -> smem res s }
   = match s.contents with Nil -> absurd | Cons h _ -> h end

   let function tail (s : state) : state
     requires { not (isEmpty s) }
    (* requires { s.contents <> Nil } *)
     ensures { tl s.contents = Some result.contents }
     ensures { match contents s with
               | Nil -> false
               | Cons _ f1 -> f1 = result.contents
               end }
     returns { res -> forall x:thread. smem x res -> smem x s }
   = match s.contents with Nil -> absurd | Cons h t -> { contents = t; elems = F.remove h s.elems } end

   let lemma non_empty_has_head (s:state)
     requires { not(isEmpty s) }
     ensures { smem (head s) s }
     =
     assert { exists t:thread. hd s.contents = Some t && Mem.mem t s.contents }

   predicate disjoint_states (a : state) (b : state) = disjoint_lists a.contents b.contents

   lemma disjoint_empty_left :  forall a b:state. isEmpty a -> disjoint_states a b

   lemma disjoint_empty_right : forall a b:state. isEmpty b -> disjoint_states a b
     
   let function slen (s: state) : int =
     returns { res -> res = Length.length s.contents }
     Length.length s.contents

   lemma slen0_not_smem : forall s:state, t:thread. slen s=0 -> not(smem t s)

   lemma empty_not_smem : forall s:state, t:thread. isEmpty s -> not(smem t s)

   predicate valid_state (a : state) = 
      distinct a.contents

   let function sload (s: state) : int =
     sumloadl s.contents

   lemma sload_slen_one : forall s:state.
     slen s = 1 -> forall t:thread. smem t s -> sload s = load t

   lemma sload_slen_multi_thread : forall s:state.
     slen s > 1 -> forall t:thread. smem t s -> sload s > load t

   lemma sload_slen_multi : forall s:state. sload s >= slen s

   let function remove_from_state (t: thread) (s:state) : state 
     requires { smem t s }
     returns  { res -> not (smem t res) }
     returns  { res -> slen res = slen s - 1 }
     returns  { res -> sload res = sload s - load t }
     returns  { res -> forall x:thread. x <> t -> smem x s -> smem x res }
     returns  { res -> forall x:thread. smem x res -> smem x s }
     returns  { res -> forall x:thread. not(smem x s) -> not(smem x res) }
     returns  { res -> forall o:state. disjoint_states s o -> disjoint_states res o } =
     { contents = remove t s.contents; elems = F.remove t s.elems }

   let function add_to_state (t: thread) (s:state) : state 
     requires { not(smem t s) }
     returns  { res -> smem t res }
     returns  { res -> slen res = slen s + 1 }
     returns  { res -> sload res = sload s + load t }
     returns  { res -> forall x:thread. x <> t -> smem x res -> smem x s }
     returns  { res -> forall x:thread. smem x s -> smem x res }
     returns  { res -> forall o:state. not (smem t o) -> disjoint_states s o -> disjoint_states res o } =
     { contents = add t s.contents; elems = F.add t s.elems }

end


