module Boolvec

use int.Int
use ref.Ref
use array.Array 
use import sumarray.SumArray as S

type boolvec = Array.array bool

let function is_true (b:bool) = if b then 1 else 0

predicate is_empty (b:bool) = not b

(*function is_empty (b:bool) : bool = not b *)

clone sumarray.SumArray as P with
  type t = bool,
  function f = is_true,
 (* function isEmpty = is_empty*)
  predicate isEmpty = is_empty

meta remove_prop lemma P.sum_right_extension
meta remove_prop lemma P.sum_transitivity

let function population (ar:boolvec) (n:int)
  requires { 0 <= n <= Array.length ar }
  =
  P.sum ar 0 n

(* This instantiates the last returns in SumMatrix.sum, but it seems to
have to be a lemma explicitly for the first loop invariant init of step2 *)
lemma positive_population : forall ar : boolvec.
  population ar (Array.length ar) > 0 ->
  exists i:int. 0 <= i < (Array.length ar) && ar[i] = true

let rec cpumask_first (stealable_cores:boolvec) : int =
  requires { population stealable_cores (Array.length stealable_cores) > 0 }
  returns { res -> 0 <= res < Array.length stealable_cores }
  returns { res -> stealable_cores[res] }
  let abort = ref false in
  let res = ref (-1) in
  for i = 0 to Array.length stealable_cores - 1 do
        invariant { !abort -> 0 <= !res < Array.length stealable_cores }
        invariant { !abort -> stealable_cores[!res] }
        invariant { not !abort -> exists j:int. i <= j < (Array.length stealable_cores) && stealable_cores[j] = true }
        if not !abort && stealable_cores[i]
        then
                begin
                        res := i;
                        abort := true
                end
  done;
  !res

predicate monotone_boolvec (oldv v:boolvec) =
  forall co:int. 0 <= co < Array.length v -> oldv[co] = true -> v[co] = true

lemma preserves_monotonicity : forall v:boolvec.
  forall co:int. 0 <= co < Array.length v -> monotone_boolvec v (v[co <- true])

lemma monotone_boolvec_transitivity : forall v1 v2 v3:boolvec.
  Array.length v1 = Array.length v2 -> Array.length v2 = Array.length v3 ->
  monotone_boolvec v1 v2 -> monotone_boolvec v2 v3 -> monotone_boolvec v1 v3
end
